## [0.0.1] - 04-03-2020

* BottomNavigationBar: Init

## [0.0.2] - 04-03-2020

* BottomNavigationBar: Optimize


## [0.0.3] - 08-03-2020

* ITabBar: Create
* BottomNavigationBar: Remove


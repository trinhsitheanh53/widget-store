import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ITabBar extends StatefulWidget {
  const ITabBar({
    Key key,
    this.tabs,
    this.hasAppBar = false,
    this.hasBottomAppBar = false,
    this.appBarHeight = 0,
    this.bottomAppBarHeight = 0,
    this.margin,
    this.tabChanged,
    this.indexDefault = 0,
    this.labelColor,
    this.unselectedLabelColor,
    this.indicatorColor,
    this.isScrolled = true,
  }) : super(key: key);

  final List<ITab> tabs;
  final bool hasAppBar;
  final bool hasBottomAppBar;
  final double appBarHeight;
  final double bottomAppBarHeight;
  final EdgeInsetsGeometry margin;
  final Function tabChanged;
  final int indexDefault;
  final Color labelColor;
  final Color unselectedLabelColor;
  final Color indicatorColor;
  final bool isScrolled;

  @override
  _ITabBarState createState() => _ITabBarState();
}

class _ITabBarState extends State<ITabBar> with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<Widget> _tabContents;
  List<Widget> _tabIconsSelected;
  List<Widget> _tabIconsUnSelected;
  List<Widget> _tabIconsContent;
  int _index;
  int _nextIndex;
  bool _isChanged;
  bool _isScrolled;
  int _amountScroll;

  @override
  void initState() {
    super.initState();
    _index = widget.indexDefault;
    _nextIndex = _index;
    _amountScroll = 0;
    _isChanged = true;
    _isScrolled = true;

    _tabController = TabController(
        vsync: this, length: widget.tabs.length, initialIndex: _index);
    _tabController.addListener(() {
      if (widget.tabChanged != null) {
        widget.tabChanged(_tabController.index);
      }
    });

    _tabContents = widget.tabs.map((iTab) => iTab.content).toList();
    _tabIconsSelected = widget.tabs.map((iTab) => iTab.iconSelected).toList();
    _tabIconsUnSelected =
        widget.tabs.map((iTab) => iTab.iconUnSelected).toList();

    _tabIconsContent = widget.tabs.map((iTab) => iTab.iconUnSelected).toList();
    _tabIconsContent[_index] = _tabIconsSelected[_index];
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changedTab(int index) {
    setState(() {
      if (_index != index) {
        _isScrolled = false;
        _amountScroll = (_tabController.index - _index).abs();
        _tabIconsContent[_index] = _tabIconsUnSelected[_index];
        _index = index;
        _tabIconsContent[_index] = _tabIconsSelected[_index];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var _appBar;
    if (widget.hasAppBar) {
      _appBar = PreferredSize(
          preferredSize: Size.fromHeight(widget.appBarHeight),
          child: Container(
              margin: widget.margin,
              color: Color(0x00000000),
              child: AppBar(
                  bottom: TabBar(
                    tabs: _tabIconsContent,
                    controller: _tabController,
                    indicatorColor: widget.indicatorColor,
                    unselectedLabelColor: widget.unselectedLabelColor,
                    labelColor: widget.labelColor,
                    onTap: changedTab,
                  ))));
    }

    var _bottomAppBar;
    if (widget.hasBottomAppBar) {
      _bottomAppBar = BottomAppBar(
          child: Container(
              height: widget.bottomAppBarHeight,
              margin: widget.margin,
              color: Color(0x00000000),
              child: TabBar(
                tabs: _tabIconsContent,
                controller: _tabController,
                indicatorColor: widget.indicatorColor,
                unselectedLabelColor: widget.unselectedLabelColor,
                labelColor: widget.labelColor,
                onTap: changedTab,
              )));
    }

    return Scaffold(
      appBar: _appBar,
      bottomNavigationBar: _bottomAppBar,
      body: NotificationListener<ScrollNotification>(
          // ignore: missing_return
          onNotification: (scrollNotification) {
            if (scrollNotification is ScrollStartNotification) {
              _isChanged = true;
            } else
            if (scrollNotification is ScrollUpdateNotification && _isScrolled) {
              setState(() {
                if (_tabController.offset > 0.2 && _index < widget.tabs.length && _isChanged) {
                  _nextIndex = _index + 1;
                  _tabIconsContent[_nextIndex] = _tabIconsSelected[_nextIndex];
                  _isChanged = false;
                } else
                if (_tabController.offset < -0.2 && _index > 0 && _isChanged) {
                  _nextIndex = _index - 1;
                  _tabIconsContent[_nextIndex] = _tabIconsSelected[_nextIndex];
                  _isChanged = false;
                }
                if (_tabController.offset.abs() > 0.9) {
                  if (_nextIndex != _index) {
                    _tabIconsContent[_index] = _tabIconsUnSelected[_index];
                    _index = _nextIndex;
                  }
                } else if (_tabController.offset.abs() < 0.1) {
                  if (_nextIndex != _index) {
                    _tabIconsContent[_nextIndex] =
                    _tabIconsUnSelected[_nextIndex];
                    _nextIndex = _index;
                  }
                }
              });
            } else if (scrollNotification is ScrollEndNotification) {
              _isChanged = true;
              if (--_amountScroll == 0) {
                _isScrolled = true;
              }
            }
          },
          child: TabBarView(
            physics: widget.isScrolled ? PageScrollPhysics() : NeverScrollableScrollPhysics(),
            children: _tabContents,
            controller: _tabController,
          )
      ),
    );
  }
}

class ITab {
  final Widget content;
  final Widget iconUnSelected;
  final Widget iconSelected;

  const ITab({this.content, this.iconUnSelected, this.iconSelected});
}
